Setting up weather app project
1.Dowload Anypoint Studio 6.6 & Mule 3.9 from this link https://www.mulesoft.com/lp/dl/studio/previous. This is a trial version requires Java7 or Java8 already installed
2. Pull project in a local system folder using command git clone https://sprakash72@bitbucket.org/sprakash72/projects.git
3. Launch Anypoint Studio and select project folder as workspace. Project should appear in package view of Anypoint Studio. If it doesnt appear,  import project from within anypoint studio
4. Right click on project and select Run as Mule Application. Mule console should display below output as application gets deployed successfully on localhost, port 8081. 
**********************************************************************
*              - - + DOMAIN + - -               * - - + STATUS + - - *
**********************************************************************
* default                                       * DEPLOYED           *
**********************************************************************

*******************************************************************************************************
*            - - + APPLICATION + - -            *       - - + DOMAIN + - -       * - - + STATUS + - - *
*******************************************************************************************************
* weatherapp                                    * default                        * DEPLOYED           *
*******************************************************************************************************

5. After deployment following URLs should produce JSON output in Postman
http://localhost:8081/services/launchwindows 
http://localhost:8081/services/launchwindows?location=Darwin
http://localhost:8081/services/launchwindows?location=Melbourne
http://localhost:8081/services/launchwindows?location=Perth