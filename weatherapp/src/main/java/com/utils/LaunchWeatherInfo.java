package com.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.launchwindows.model.weatherinfo.LaunchWindow;
import com.launchwindows.model.weatherinfo.LaunchWindows;
import com.launchwindows.model.weatherinfo.WeatherInfo;
import com.launchwindows.model.weatherinfo.WeatherParameters;

public class LaunchWeatherInfo {

	List<LaunchWindow> launchWindowlist = new ArrayList();
	
	public String getCityCode(String city) {
		String locationId = Constants.citytocodemap.get(city);

		return locationId;
	}
	
	public List<String> getCityCodeList() {
		List<String> cities = new ArrayList();
		for (Map.Entry<String, String> entry : Constants.citytocodemap.entrySet()) {
			String city = entry.getValue();
			cities.add(city);
		}
		return cities;
	}
	
	public Map<String,String> getCityCodeMap() {
		return Constants.citytocodemap;
	}
		


	public void processWindowResults(WeatherInfo locResponse, String city) {
		List<WeatherParameters> list = locResponse.getList();
		for (int i = 0; i < list.size(); ++i) {
			WeatherParameters params = list.get(i);
			Float temp = params.getMain().getTemp();
			Float windDir = params.getWind().getDeg();
			Float windSpeed = params.getWind().getSpeed();
			int clouds = params.getClouds().getAll();
			String datetime = params.getDt_txt();

			if (IsWindowValid(city, clouds, windSpeed) == false)
				continue;

			int launchWindowScore = GetLaunchScore(temp, windSpeed, windDir);

			LaunchWindow launchWindow = new LaunchWindow();
			launchWindow.setLocation(city);
			launchWindow.setDatetime(datetime);
			launchWindow.setScore(launchWindowScore);

			launchWindowlist.add(launchWindow);

//        	System.out.println(datetime + " " + temp + " " + windDir + " "+ windSpeed + " " + clouds);
		}


	}
	
	boolean IsWindowValid(String city, int clouds, Float windSpeed) {
		if (Constants.cloudinessMap.containsKey(city) && clouds > Constants.cloudinessMap.get(city))
			return false;

		if (Constants.windspeedMap.containsKey(city) && clouds > Constants.windspeedMap.get(city))
			return false;

		return true;

	}
	
	int GetLaunchScore(Float temp, Float windSpeed, Float windDir) {
		return (int) (Math.abs(20 - temp) + windSpeed + Math.abs(220 - windDir) * 0.1);
	}
	
	public LaunchWindows collectAndSort(int maxsize) {
		// Sort WindowList on Score
		Collections.sort(launchWindowlist);
		LaunchWindows launchWindows = new LaunchWindows();

		// Create a sublist of at most top 5 results and add to window list
		launchWindows.setLaunchWindows(launchWindowlist.subList(0, Math.min(launchWindowlist.size(), maxsize)));

		return launchWindows;
	}
	
	
}
