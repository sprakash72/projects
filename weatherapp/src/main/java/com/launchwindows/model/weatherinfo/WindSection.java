package com.launchwindows.model.weatherinfo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class WindSection {
	Float speed;
	Float deg;

	public Float getDeg() {
		return deg;
	}

	public void setDeg(Float deg) {
		this.deg = deg;
	}

	public Float getSpeed() {
		return speed;
	}

	public void setSpeed(Float speed) {
		this.speed = speed;
	}
	
}
